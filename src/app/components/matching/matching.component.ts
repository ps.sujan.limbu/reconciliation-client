import { Component, OnInit } from '@angular/core';
import { ResultData } from 'src/app/models/result-data.model';
import { ResultService } from 'src/app/services/result.service';

@Component({
  selector: 'app-matching',
  templateUrl: './matching.component.html',
  styleUrls: ['./matching.component.css']
})
export class MatchingComponent implements OnInit {
  responseData: string[] = [];
  headers: string[] = [];
  errorMessage: null;
  constructor(private resultService: ResultService) { }

  ngOnInit(): void {
    this.resultService.getMatchingResult()
    .subscribe((response: string[]) => {
      this.responseData = response;
      this.headers = this.responseData[0].split(",");
      this.responseData.shift();
    }, error =>{
      this.errorMessage = error.error.message;
    });
  }

}
