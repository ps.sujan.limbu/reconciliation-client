import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Result } from 'src/app/models/result.model';
import { ReconciliationService } from 'src/app/services/reconciliation.service';

@Component({
  selector: 'app-reconciliation',
  templateUrl: './reconciliation.component.html',
  styleUrls: ['./reconciliation.component.css']
})
export class ReconciliationComponent implements OnInit {
  isReconciliationCompleted = false;
  formData: FormData = new FormData();
  errorMessage = null;
  sourceFileErrorMessage= '';
  isSourceFileValid = true;
  isTargetFileValid = true;
  targetFileErrorMessage = '';

  constructor(private route: Router, private reconciliationService: ReconciliationService){}
  
  ngOnInit(): void {
  }

  onSourceSelect(event: any): void {
    if (event.target.files.length > 0) {
      const file = event.target.files[0];
      if(!this.isFileFormatValid(file)){
        this.sourceFileErrorMessage = "Invalid file format";
        this.isSourceFileValid = false;
      }else{
        this.isSourceFileValid = true;
        if(this.formData.has('sourceFile'))
          this.formData.delete('sourceFile');
        this.formData.append('sourceFile', file);
      }
    }
  }
  
  onTargetSelect(event: any): void {
    if (event.target.files.length > 0) {
      const file = event.target.files[0];
      if(!this.isFileFormatValid(file)){
        this.targetFileErrorMessage = "Invalid file format";
        this.isTargetFileValid = false;
      }else{
        this.isTargetFileValid = true;
        if(this.formData.has('targetFile'))
          this.formData.delete('targetFile');
        this.formData.append('targetFile', file);
      }
    }
  }

  isFileFormatValid(file: any): boolean {  
    var allowedExtensions = ["csv","json"];
    const fileExtension = file.name.split('.').pop();
    return allowedExtensions.includes(fileExtension.toLowerCase());
  }

  doReconcile(){
    if(this.formData.has('sourceFile') && this.formData.has('targetFile')){
      this.reconciliationService.reconcile(this.formData).subscribe((data: Result[]) =>{ 
        data.forEach(entry => console.log(entry.fileName));
        this.isReconciliationCompleted = true;
        this.errorMessage = null;
        this.route.navigate(['/matching']);
       }, err =>{
         this.errorMessage = err.error.message;
         this.isReconciliationCompleted =false;
       });
    }
  }
  
  close(){
    this.isReconciliationCompleted = false;
  }


}
