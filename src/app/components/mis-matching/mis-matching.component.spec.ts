import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MisMatchingComponent } from './mis-matching.component';

describe('MisMatchingComponent', () => {
  let component: MisMatchingComponent;
  let fixture: ComponentFixture<MisMatchingComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MisMatchingComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MisMatchingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
