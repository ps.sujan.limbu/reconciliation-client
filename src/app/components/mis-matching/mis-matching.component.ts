import { Component, OnInit } from '@angular/core';
import { ResultService } from 'src/app/services/result.service';

@Component({
  selector: 'app-mis-matching',
  templateUrl: './mis-matching.component.html',
  styleUrls: ['./mis-matching.component.css']
})
export class MisMatchingComponent implements OnInit {
  responseData: string[] = [];
  headers: string[] = [];
  errorMessage: null;
  constructor(private resultService: ResultService) { }

  ngOnInit(): void {
    this.resultService.getMisMatchingResult()
    .subscribe((response: string[]) => {
      this.responseData = response;
      this.headers = this.responseData[0].split(",");
      this.responseData.shift();
    }, error =>{
      this.errorMessage = error.error.message;
    });    
  }  

}
