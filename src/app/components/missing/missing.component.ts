import { Component, OnInit } from '@angular/core';
import { ResultService } from 'src/app/services/result.service';

@Component({
  selector: 'app-missing',
  templateUrl: './missing.component.html',
  styleUrls: ['./missing.component.css']
})
export class MissingComponent implements OnInit {
  responseData: string[] = [];
  headers: string[] = [];
  errorMessage: null;

  constructor(private resultService: ResultService) { }

  ngOnInit(): void {
    this.resultService.getMissingResult()
    .subscribe((response: string[]) => {
      this.responseData = response;
      this.headers = this.responseData[0].split(",");
      this.responseData.shift();
    }, error =>{
      this.errorMessage = error.error.message;
    });
  }
}
