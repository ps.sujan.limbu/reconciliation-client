import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import {HttpClientModule} from '@angular/common/http'

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { MatchingComponent } from './components/matching/matching.component';
import { MissingComponent } from './components/missing/missing.component';
import { MisMatchingComponent } from './components/mis-matching/mis-matching.component';
import { FormsModule } from '@angular/forms';
import { ReconciliationComponent } from './components/reconciliation/reconciliation.component';
import { authInterceptorProviders } from './helper/auth.interceptor';

@NgModule({
  declarations: [
    AppComponent,
    MatchingComponent,
    MissingComponent,
    MisMatchingComponent,
    ReconciliationComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [authInterceptorProviders],
  bootstrap: [AppComponent]
})
export class AppModule { }
