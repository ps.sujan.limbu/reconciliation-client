import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AppComponent } from './app.component';
import { MatchingComponent } from './components/matching/matching.component';
import { MisMatchingComponent } from './components/mis-matching/mis-matching.component';
import { MissingComponent } from './components/missing/missing.component';
import { ReconciliationComponent } from './components/reconciliation/reconciliation.component';

const routes: Routes = [
  {path:"recon", component:ReconciliationComponent},
  {path:"matching", component:MatchingComponent},
  {path:"mis-matching", component:MisMatchingComponent},
  {path:"missing", component:MissingComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }