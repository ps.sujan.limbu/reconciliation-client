export interface ErrorResponse {
    message:  string;
    statusCode: number;
    dateTime: string;
}
