import { Link } from "./link.model";

export interface Result {
    fileName: string;
    links:    Link[];
}