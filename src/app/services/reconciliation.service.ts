import { HttpClient,HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

const API_URL = 'http://localhost:8080/api/v1/reconciliation/'; 

@Injectable({
  providedIn: 'root'
})
export class ReconciliationService {

  constructor(private http: HttpClient) { }

  reconcile(formData: FormData): Observable<any>{
    formData.forEach(data => console.log(data));
    return this.http.post(API_URL+'import/files', formData);
  }
}
