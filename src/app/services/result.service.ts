import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

const API_URL = 'http://localhost:8080/api/v1/result/'; 

@Injectable({
  providedIn: 'root'
})
export class ResultService {

  constructor(private http: HttpClient) { }

  getMatchingResult(): Observable<any>{
    return this.http.get(API_URL+'matching');
  }
  getMisMatchingResult(): Observable<any>{
    return this.http.get(API_URL+'mis-matching');

  }
  getMissingResult(): Observable<any>{
    return this.http.get(API_URL+'missing');
  }
}
