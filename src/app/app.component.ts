import { Component } from '@angular/core';
import { ErrorResponse } from './models/error.response';
import { AuthService } from './services/auth.service';
import { TokenService } from './services/token.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'reconciliation-client';

  isLoggedIn: boolean = false;
  loggedUsername = null;
  errorMessage = null;
  loginForm: any ={
    username:null,
    password:null,
  };

  constructor(private authService: AuthService, private tokenService: TokenService){}

  ngOnInit(): void {
    if(this.tokenService.getToken()){
      this.isLoggedIn = true;
      this.loggedUsername = this.tokenService.getUser().username;
    }
  }

  onLogin(){
    const {username, password} = this.loginForm;
    if (username!=null && password!=null){
      this.authService.login(username, password).subscribe(response =>{
        this.tokenService.saveToken(response.token);
        this.tokenService.saveUser({username:response.username, id:response.id})
        this.loggedUsername = response.username;
        this.isLoggedIn = true;
        this.errorMessage = null;
      },error =>{
        this.errorMessage = error.error.message;
      });
    }
  }

  onLogout(){
    this.tokenService.clear();
    this.isLoggedIn = false;
  }
}

