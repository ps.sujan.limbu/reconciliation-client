import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HTTP_INTERCEPTORS } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { TokenService } from "../services/token.service";

const TOKEN_HEADER_KEY = "Authorization";
@Injectable()
export class AuthInterceptor implements HttpInterceptor {
    constructor(private tokenService: TokenService){}

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        let tokenizedReq = req;
        const token = this.tokenService.getToken();
        if(token!=null){
            tokenizedReq = req.clone({headers:req.headers.set(TOKEN_HEADER_KEY, 'Bearer '+token)});
        }
        return next.handle(tokenizedReq);
    }

}

export const authInterceptorProviders = [{provide:HTTP_INTERCEPTORS, useClass:AuthInterceptor, multi:true}];